import java.util.Scanner;

//Escreva um c�digo  para Ler tr�s n�meros inteiros e mostrar o maior e o menor deles.
public class ComparaTresNumeros {

	public static void main(String[] args) {
		int x, y, z;
		String result;
		
		Scanner s = new Scanner(System.in);
		System.out.println("Digite um valor para x:");
		x = s.nextInt();
		System.out.println("Digite um valor para y:");
		y = s.nextInt();
		System.out.println("Digite um valor para z:");
		z = s.nextInt();
		
		result = comparaTresNumeros(x, y, z);
		System.out.println(result);

	}
	
/*
 * 			Arrumar a valida��o!
 */
	public static String comparaTresNumeros(int x, int y, int z) {
		int maiorNumero = 0;
		int menorNumero = 0;
		String result;

		if(x > y && x > z) {
			maiorNumero = x;
		} else if(y > x && y > z) {
			maiorNumero = y;
		} else {
			maiorNumero = z;
		}
		
		if (x < y && x < z) {
			menorNumero = x;
		} else if(y < x && y < z) {
			menorNumero = y;
		} else {
			menorNumero = z;
		}
		
		result = "menor numero = " + menorNumero + " e maior numero = " + maiorNumero;

		return result;
	}

}
