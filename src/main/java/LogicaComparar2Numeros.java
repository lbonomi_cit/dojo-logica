import java.util.Scanner;

public class LogicaComparar2Numeros {

	public static void main(String[] args) {
		
		int x;
		int y;
		String result;
		
		Scanner s = new Scanner(System.in);
		System.out.println("Digite a variavel x:");
		x = s.nextInt();

		System.out.println("Digite a variavel y:");
		y = s.nextInt();
		
		result = comparaNumeros(x, y);
		
		System.out.println(result);
		

	}
	
	public static String comparaNumeros(int x, int y) {
		String result;
		if(x > y) {
			result = "x � maior que y";
		} else if (y > x) {
			result = "y � maior que x";
		} else {
			result = "x e y s�o iguais";
		}
		return result;
	}

}
