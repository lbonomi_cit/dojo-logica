import java.util.Scanner;

public class AreaTriangulo {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		float base;
		float altura;
		float area;
		
		System.out.println("Digite a base:");
		base = s.nextFloat();

		System.out.println("Digite a altura:");
		altura = s.nextFloat();
		
		area = calculaArea(base, altura);
	}
	
	public static float calculaArea(float base, float altura) {
		float result = (base * altura) / 2; 
		System.out.println("a area �: " + result);
		return result;
	}
}